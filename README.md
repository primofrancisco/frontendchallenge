## Frontend Challenge - Documentation

This script is intended to work as a stand-alone JavaScript file, meaning no other libraries, frameworks or special environments are required.

The objective is to connect to an API in order to retrieve data and display it on any given page. For showcasing purposes, a simple index.html was created, only having a #root div, in which the script will create the required markup for displaying the desired content. Also a style.css is linked, to achieve the expected visual result and to improve the user experience.

In order to make the HTTP request, we create a variable and assign a new XMLHttpRequest object to it. Then we'll use the open() method to open a new connection, specifying in the arguments the type of request and the URL of the API endpoint. We can now access the data inside the onload function. We use JSON.parse() to parse the response, and create a jsondata variable that contains all the JSON as an array of JavaScript objects. Using an if condition, we can check for the HTTP request response, and display a console.log error if necessary. With no errors found on the request, we will access the needed values, and store them in their respective variables, which we will then use in the different html elements we create. After appending everything we need, we’ll send the request.

---

Primo Francisco