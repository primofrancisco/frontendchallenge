var request = new XMLHttpRequest();

request.open('GET', 'https://api.olapic.com/media/2899395330?auth_token=0a40a13fd9d531110b4d6515ef0d6c529acdb59e81194132356a1b8903790c18&version=v2.2', true);
request.onload = function () {

  var jsondata = JSON.parse(this.response);

  if (request.status >= 200 && request.status < 400) {
    var dataImage = jsondata.data.images.normal;
    var dataDate = jsondata.data.date_published;
    var dataUsername = jsondata.data._embedded.uploader.username;
    var dataAvatar = jsondata.data._embedded.uploader.avatar_url;
  } else {
    console.log('HTTP request error');
  }

  const app = document.getElementById('root');

  const demoWrapper = document.createElement('div');
  demoWrapper.setAttribute('class', 'demo--wrapper');

  const demoAvatar = document.createElement('img');
  demoAvatar.setAttribute('class', 'demo--avatar');
  demoAvatar.src = dataAvatar;

  const demoUsername = document.createElement('h4');
  demoUsername.setAttribute('class', 'demo--username');
  demoUsername.textContent = dataUsername;

  const demoImageCont = document.createElement('div');
  demoImageCont.setAttribute('class', 'demo--image-cont');

  const demoImage = document.createElement('img');
  demoImage.setAttribute('class', 'demo--image');
  demoImage.src = dataImage;

  const demoDate = document.createElement('p');
  demoDate.setAttribute('class', 'demo--date');
  demoDate.textContent = dataDate.substring(0, 10);

  app.appendChild(demoWrapper);
  demoWrapper.appendChild(demoAvatar);
  demoWrapper.appendChild(demoUsername);
  demoWrapper.appendChild(demoImageCont);
  demoImageCont.appendChild(demoImage);
  demoWrapper.appendChild(demoDate);

}

request.send();
